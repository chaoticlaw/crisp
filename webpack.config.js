const path = require('path');

module.exports = {
  module: {
    rules: [{
      test: /\.min\.js$/,
      use: ['script-loader']
    }]
  },
  entry: './src/js/main.js',
  mode: 'production',
  output: {
    filename: 'main.js',
    path: path.resolve(__dirname, 'assets/js'),
  }
}
