import './pancake.min.js';
import './highlight.min.js';
import 'instant.page';
// var Turbolinks = require('turbolinks');
// Turbolinks.start();

if (document.readyState === 'loading') {
  document.addEventListener('DOMContentLoaded', function() {
    hljs.initHighlighting();
  });
} else {
  hljs.initHighlighting();
}
